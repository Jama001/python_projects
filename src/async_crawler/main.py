import asyncio
import sys
import random
import string
import requests

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

async def crawl(group, urls):
    taskid = id_generator()
    for url in urls:
        u = f"http://{url}"
        print(f"Task group: {group}. Task id: {taskid}. Url: {u}")
        await asyncio.sleep(0.5)
        result = requests.get(u)
        print(f"Task id: {taskid}. Result: {result.status_code}")


async def main(filename=None):
    try:
        urls = []
        with open(filename, mode="r", encoding="utf8") as r:
            for u in r.read().splitlines():
                urls.append(u)

        await asyncio.gather(
            crawl("A", urls[0:5]),
            crawl("B", urls[5:10]),
            crawl("C", urls[10:]),
        )
    except FileNotFoundError as e:
        print(f"file not found. {filename}")
    except Exception as e:
        print(e)


if __name__ == '__main__':
    filename = sys.argv[1]
    asyncio.run(main(filename))