import matplotlib
matplotlib.use('TkAgg') 
import matplotlib.pyplot as plt
from pandas import read_csv 
import math

def standardeviation(variance):
    """Standard Deviation is the square root of Variance"""
    return variance**0.5

def mean(values):
    """Calculate the mean value of a list of numbers"""
    return sum(values) / float(len(values))

def variance(values, mean, datasize):
    """The average of the squared differences from the Mean.

    Variance for a list of numbers can be calculated as: variance = sum( (x - mean(x))^2 )
    """
    return (1/datasize) * sum([(x-mean)**2 for x in values])

def main(dataset):
    x = [row[0] for row in dataset]
    y = [row[1] for row in dataset]   
    print(len(dataset))
    mean_x, mean_y = mean(x), mean(y)
    var_x, var_y = variance(x, mean_x, len(dataset)), variance(y, mean_y, len(dataset))
    std_x, std_y = standardeviation(var_x), standardeviation(var_y) 
    print('x stats: mean=%.3f variance=%.3f std=%.3f' % (mean_x, var_x, std_x))
    print('y stats: mean=%.3f variance=%.3f std=%.3f' % (mean_y, var_y, std_y))

def plot_data(dataset):
    x = [row[0] for row in dataset]
    y = [row[1] for row in dataset]   

    plt.scatter(x, y, alpha=0.5)
    plt.title('Scatter plot')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.savefig('plots/linreg.png')
    plt.show()

if __name__=='__main__':
    i = 1
    data = read_csv('./datasets/linreg1.csv')
    dataset = [[x, y] for x, y in zip(data['A'], data['B'])]
    print(dataset)
    main(dataset)
    # plot_data(dataset)